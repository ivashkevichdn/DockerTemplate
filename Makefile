
#########################################################################
#							Основные цели.								#
#########################################################################

#
all: build test

# Запуск mydjango.org.
run: run_backend

# Остановить mydjango.org.
stop: stop_backend

# Сборка всего проекта как BackEnd`а, так и FrontEnd`а.
build: clean build_backend collectstatic migrate

# Запуск полной проверки BackEnd`а и FrontEnd`а.
check: check_backend

# Запуск всех тестов BackEnd`а и FrontEnd`а.
test: test_backend

# Очистка BackEnd`а и FrontEnd`а.
clean: clean_backend



#########################################################################
#					Цели для работы с BackEnd`ом.						#
#########################################################################

PATH_BACKEND = ./backend
DOCKER_COMPOSE = docker-compose -f docker-compose.yml

# Запуск BackEnd`а.
run_backend:
	$(DOCKER_COMPOSE) up -d

# Остановка BackEnd`а.
stop_backend:
	$(DOCKER_COMPOSE) down -v

# Сборка Docker контейнеров для BackEnd`а.
build_backend:
	$(DOCKER_COMPOSE) build
	

# Запуск проверки BackEnd`а.
check_backend:
	@echo "ERROR: Проверка BackEndа не реализованна!"
	@exit 1

# Запуск тестов BackEnd`а.
test_backend:
	@echo "ERROR: Тесты BackEndа не реализованны!"
	@exit 1

# Очистка BackEnd`а.
clean_backend:
	find . -type f -name "*.pyc" -delete
	find . -type d -name "__pycache__" -delete


# Создание новых миграций на основе изменений, которые вы внесли в свои модели на стороне BackEnd`а.
makemigrations:
	$(DOCKER_COMPOSE) run --rm backend ./manage.py makemigrations

# Применение миграций, а также за отмену подачи и перечисления их статуса на стороне BackEnd`а.
migrate:
	$(DOCKER_COMPOSE) run --rm backend ./manage.py migrate --no-input

# Выполним подготовку/сборку статических файлов.
collectstatic:
	$(DOCKER_COMPOSE) run --rm backend ./manage.py collectstatic --no-input

# Запустить очистку Docker контейнеров.
dockerclean:
	docker system prune -f
